<?php
  // définir la fonction pour déterminer la valeur d'affichage du trimestre
  function setTrimestre ($numTrimestre) {
    switch ($numTrimestre) {
      case 1:
        return "Hiver";
      case 2:
        return "Été";
      case 3:
        return "Automne";
      default:
        return "INVALIDE";
    }
  }
  // récupérer les informations de chaque cours à partir du fichier de cours
  $cours = array_map('rtrim', file('./cours.txt'));
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8"/>
    <title>INF2005 - Lab3</title>
  </head>
  <body>
    <h1>Liste de cours</h1>
    <table>
      <thead>
        <tr>
          <th>Sigle du cours</th>
          <th>Groupe</th>
          <th>Année</th>
          <th>Trimestre</th>
          <th>Titre du cours</th>
        </tr>
      </thead>
      <tbody>
        <?php
          $nbCours = 0;
          foreach($cours as $cour) {
            // isoler chaque information d'un cours
            $annee = substr($cour,0,4);
            $numTrimestre = substr($cour,4,1);
            $sigle = substr($cour,6,7);
            $groupe = substr($cour,14,2);
            $titre = substr($cour,17);
            // déterminer la valeur d'affichage du trimestre
            $trimestre = setTrimestre($numTrimestre);
        ?>
        <tr>
          <td><?= $sigle; ?></td>
          <td><?= $groupe; ?></td>
          <td><?= $annee; ?></td>
          <td><?= $trimestre; ?></td>
          <td><?= $titre; ?></td>
        </tr>
        <?php
            $nbCours++;
          } // fin du foreach
        ?>
      </tbody>
    </table>
    <p>Il y a <?= $nbCours; ?> cours dans le tableau.</p>
  </body>
</html>