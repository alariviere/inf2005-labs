<?php
  // définir la fonction pour déterminer la valeur d'affichage de la session
  function setTrimestre ($numTrimestre) {
    switch ($numTrimestre) {
      case 1:
        return "Hiver";
      case 2:
        return "Été";
      case 3:
        return "Automne";
      default:
        return "INVALIDE";
    }
  }
  // récupérer les informations de chaque cours à partir du fichier de cours
  $cours = array_map('rtrim', file('./cours.txt'));
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8"/>
    <title>INF2005 - Lab3</title>
  </head>
  <body>
    <h1>Liste de cours</h1>
    <table>
      <thead>
        <tr>
          <th>Sigle du cours</th>
          <th>Groupe</th>
          <th>Année</th>
          <th>Trimestre</th>
          <th>Titre du cours</th>
        </tr>
      </thead>
      <tbody>
        <?php
          $nbCours = 0;
          foreach($cours as $cour) {
            echo "<tr>";
            echo "<td>" . substr($cour,6,7) . "</td>";
            echo "<td>" . substr($cour,14,2) . "</td>";
            echo "<td>" . substr($cour,0,4) . "</td>";
            echo "<td>" . setTrimestre(substr($cour,4,1)) . "</td>";
            echo "<td>" . substr($cour,17) . "</td>";
            echo "</tr>";
            $nbCours++;
          }  
        ?>
      </tbody>
    </table>
    <p>Il y a <?= $nbCours; ?> cours dans le tableau.</p>
  </body>
</html>