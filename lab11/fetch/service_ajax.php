<?php
$query = $_GET['q'];
if(isset($query)) {
  echo "<ul>";
  foreach (file("./cours.txt") as $line) {
    $trimmedValue = trim($line);
    if (strpos(strtolower($trimmedValue), strtolower($query)) !== false) {
      echo "<li>{$trimmedValue}</li>";
    }
  }
}
echo "</ul>"
?>