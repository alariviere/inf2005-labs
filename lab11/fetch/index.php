<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <title>INF2005 - Laboratoire Ajax</title>
    <script src="script.js"></script>
  </head>
  <body>
    <h1>Ajax</h1>
      <input id="q" name="q" placeholder="Recherche..." />
      <input id="rechercher" type="submit" />
    <div id="resultat"></div>
    <script>
      let element = document.getElementById("rechercher");
      element.addEventListener("click", chargerListeCours);
    </script>
  </body>
</html>