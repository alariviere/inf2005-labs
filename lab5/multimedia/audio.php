<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<title>INF2005 - Laboratoire 5</title>
	</head>
	<body>
		<h1>Multimédia - Audio</h1>
		<!-- Si le fichier midi ne peut être joué, télécharger une extension pour le navigateur web utilisé :
		     https://jazz-soft.net/download/midi-player/ -->
		<audio controls="controls" autoplay="autoplay">
			<source src="media/batman.mid" type="audio/midi" />
			Ce fureteur est trop vieux pour la balise audio.
		</audio>
	</body>
</html>