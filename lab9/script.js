// valider le formulaire côté client
function viderMessagesErreur() {
  erreur = false;
  document.getElementById('erreurNom').innerHTML = "";
  document.getElementById('erreurPrenom').innerHTML = "";
  document.getElementById('erreurDate').innerHTML = "";
  document.getElementById('erreurCode').innerHTML = "";
  document.getElementById('erreurGenre').innerHTML = "";
}

function validerNonVide(idInput, idMsg) {
  const msgErr1 = 'Le champ ';
  const msgErr2 = ' est requis.';
  if(document.getElementById(idInput).value == "") {
    document.getElementById(idMsg).innerHTML = msgErr1 + idInput + msgErr2;
    erreur = true;
    return false;
  }
  return true;
}

function validerGenre(idInput, idMsg) {
  const msgErr1 = 'La valeur du champ ';
  const msgErr2 = ' doit être M ou F.';
  let genre = document.getElementById(idInput).value;
  if (genre != 'M' && genre != 'F') {
    document.getElementById(idMsg).innerHTML = msgErr1 + idInput + msgErr2;
    erreur = true;
    return false;
  }
  return true;
}

function validerFormatDate(idInput, idMsg) {
  const msgErr1 = 'Le champ ';
  const msgErr2 = ' doit être au format AAAA-MM-JJ.';
  let date = document.getElementById(idInput).value;
  let dateValide = true;
  if(date.length !== 10){
    document.getElementById(idMsg).innerHTML = msgErr1 + idInput + msgErr2;
    dateValide = false;
    erreur = true;
  }
  let i = 0;
  while(dateValide && i < date.length) {
    if(i == 4 || i == 7) {
      if (date[i] !== '-') {
        document.getElementById(idMsg).innerHTML = msgErr1 + idInput + msgErr2;
        dateValide = false;
        erreur = true;
      }
    } else if (isNaN(date[i])) {
      document.getElementById(idMsg).innerHTML = msgErr1 + idInput + msgErr2;
      dateValide = false;
      erreur = true;
    }
    i++;
  }
  return dateValide;
}

function validerCode(idInput, idMsg) {
  const msgErr1 = 'Le champ ';
  const msgErr2 = ' ne contient pas la valeur attendue.';
  let codeValide = true;
  let code = document.getElementById(idInput).value;
  let codeFamille = code.substr(0, 3);
  let codePrenom = code.substr(3, 1);
  let codeJour = code.substr(4, 2);
  let codeMois = code.substr(6, 2);
  let codeAnnee = code.substr(8, 2);
  let codeSequence= code.substr(10, 2);
  let nomFamille = document.getElementById('nom').value.substr(0, 3).toUpperCase();
  let prenomPrenom = document.getElementById('prenom').value.substr(0, 1).toUpperCase();
  let dateJour = document.getElementById('date').value.substr(8, 2);
  let dateMois = document.getElementById('date').value.substr(5, 2);
  let dateAnnee = document.getElementById('date').value.substr(2, 2);
  if(code.length !== 12) {
    document.getElementById(idMsg).innerHTML = 'Le format du code permanent n\'est pas respecté.';
    erreur = true;
    codeValide = false;
  } else if (codeFamille !== nomFamille) {
    document.getElementById(idMsg).innerHTML = 'Le code permanent doit commencé par les trois premières lettres du nom de famille en majuscule.';
    erreur = true;
    codeValide = false;
  } else if (codePrenom !== prenomPrenom) {
    document.getElementById(idMsg).innerHTML = 'Le quatrième caractère du code permanent doit être la première lettre du prénom en majuscule.';
    erreur = true;
    codeValide = false;
  } else if (codeJour != dateJour) {
    document.getElementById(idMsg).innerHTML = 'Les cinquième et sixième caractères du code permanent doivent correspondre au jour de naissance.';
    erreur = true;
    codeValide = false;
  } else if (codeAnnee != dateAnnee) {
    document.getElementById(idMsg).innerHTML = 'Les neuvième et dixième caractères du code permanent doivent correspondre aux deux dernièrs chiffres de l\'année de naissance.';
    erreur = true;
    codeValide = false;
  } else {
    if (document.getElementById('genre').value == 'F') {
    dateMois = parseInt(dateMois) + 50;
    console.log(dateMois);
    }
    if (codeMois != dateMois) {
    document.getElementById(idMsg).innerHTML = 'Les septième et huitième caractères du code permanent doivent correspondre au mois de naissance.';
    erreur = true;
    codeValide = false;
    }
  }  
  return codeValide;
}

function validerSaisie(inscription) {
  viderMessagesErreur();
  const msgErr1 = 'Le champ ';
  const msgErr2 = ' est requis.';
  let nomValide = validerNonVide('nom', 'erreurNom');
  let prenomValide = validerNonVide('prenom', 'erreurPrenom');
  let dateValide = validerNonVide('date', 'erreurDate');
  let codeValide = validerNonVide('code', 'erreurCode');
  let genreValide = validerNonVide('genre', 'erreurGenre');
  if (dateValide) { dateValide = validerFormatDate('date', 'erreurDate'); }
  if (genreValide) { genreValide = validerGenre('genre', 'erreurGenre'); }
  if(codeValide && nomValide && prenomValide && dateValide && genreValide) { console.log('valide code'); validerCode('code', 'erreurCode'); }
  return erreur;
}

document.querySelector('form').addEventListener('submit', function (evt) {
  evt.preventDefault() ;
  const form = document.getElementById('formulaire')
  fetch('creer.php', { method: 'POST', body: new FormData(form) })
  .then(function (inscription) {
    let erreur = validerSaisie(inscription);
    if (inscription.ok && !erreur) {
      form.submit(); 
    } else {
      evt.preventDefault();
    }
  })
})
