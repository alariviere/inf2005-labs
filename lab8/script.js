// Lorsqu'on clique sur le bouton, les paragraphes changent de police de caractères.
document.getElementById('police').addEventListener('click', function(evt) {
  let paragraphes = document.getElementsByTagName('p');
  let paragraphe;
  for (let i = 0; i < paragraphes.length; i++) {
    paragraphe = paragraphes[i];
    paragraphe.style.font != '16px arial, sans-serif' ? 
        paragraphe.style.font = '16px arial, sans-serif' : 
        paragraphe.style.font = '16px Times New Roman, serif';
  }});

// Lorsqu'on clique sur le bouton, le premier paragraphe devient le troisième; le deuxième devient le premier et le troisième devient le deuxième.
document.getElementById('position').addEventListener('click', function(evt) {
  let element = document.getElementById('texte');
  let paragraphes = document.getElementsByTagName('p');
  let p1 = paragraphes[0];
  let p2 = paragraphes[1];
  let p3 = paragraphes[2];
  element.removeChild(p1);
  element.removeChild(p2);
  element.removeChild(p3);
  element.insertBefore(p2, paragraphes[0]);
  element.insertBefore(p3, paragraphes[1]);
  element.insertBefore(p1, paragraphes[2]);
});

// Lorsqu'on clique sur le bouton, ça fait disparaître le titre s'il est visible. Si le titre n'est pas visible, ça le fait apparaître.
document.getElementById('affichage').addEventListener('click', function(evt) {
  document.getElementById('titre').style.display === 'none' ?
      document.getElementById('titre').style.display = 'block' :
      document.getElementById('titre').style.display = 'none';
  });

// Lorsqu'on clique sur le titre, il doit devenir rouge.
document.getElementById('titre').addEventListener('click', function(evt) {
  document.getElementById('titre').style.color = 'red';
});

// Lorsqu'on clique sur l'image, ça ajoute un paragraphe de Lopem Ipsum à la base.
document.getElementById('image').addEventListener('click', function(evt) {
  let paragraphe = document.createElement("p");
  let contenu = document.createTextNode("Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
      + "Suspendisse lacinia elementum bibendum. Ut iaculis nunc quis lorem pretium condimentum. "
      + "Nam vel libero pretium, pretium nisi eget, cursus nisi. "
      + "Aenean maximus tortor sit amet ligula consequat tincidunt. Integer eu enim in libero efficitur rhoncus et nec odio. "
      + "In eleifend id augue eget semper. Nulla neque tellus, pellentesque eget interdum non, condimentum sed nulla. "
      + "Suspendisse congue id dui vel pretium. Sed quis risus pretium, tristique nulla at, sagittis mi. "
      + "Ut nunc felis, convallis et turpis in, fringilla luctus nulla. Vivamus feugiat tincidunt convallis. "
      + "In placerat lectus a est fringilla, mollis maximus nibh ultricies. "
      + "Fusce orci augue, ultricies non varius vel, fringilla venenatis nibh.");
  paragraphe.appendChild(contenu);
  let paragraphes = document.getElementsByTagName('p');
  paragraphes[0].style.font === '16px arial, sans-serif' ? 
        paragraphe.style.font = '16px arial, sans-serif' : 
        paragraphe.style.font = '16px Times New Roman, serif';
  let element = document.getElementById('texte');
  element.appendChild(paragraphe);
});