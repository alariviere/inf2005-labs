<?php
  // valider les champs du formulaire et stocker les messages d'erreur associés s'il y a lieu
  function valider ($membre_nouveau) {
    $erreurs = [];
    if (!$membre_nouveau['nom']) { $erreurs['nom'] = "Le champ 'Prénom' est obligatoire."; }
    if (!$membre_nouveau['prenom']) { $erreurs['prenom'] = "Le champ 'Prénom' est obligatoire."; }
    if (!$membre_nouveau['age']) { $erreurs['age'] = "Le champ 'Âge' est obligatoire."; }
    elseif (!is_numeric($membre_nouveau['age'])) { $erreurs['age'] = "L'âge doit contenir uniquement des chiffres."; }
    if (!$membre_nouveau['anneeDiplome']) { $erreurs['anneeDiplome'] = "Le champ 'Année de diplomation' est obligatoire."; }
    if (!$membre_nouveau['programme']) { $erreurs['programme'] = "Le champ 'Nom du programme' est obligatoire."; }
    return $erreurs;
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // stocker dans un array les valeurs saisies dans les différents champs du formulaire
    $membre_nouveau = [
      'nom' => $_POST['nom'] ?? '',
      'prenom' => $_POST['prenom'] ?? '',
      'age' => $_POST['age'] ?? '',
      'anneeDiplome' => $_POST['anneeDiplome'] ?? '',
      'universite' => $_POST['universite'] ?? '',
      'programme' => $_POST['programme'] ?? '',
      'type' => $_POST['type'] ?? '',
      'type' => $_POST['type'] ?? '',
    ];
    // valider les entrées
    $erreurs = valider($membre_nouveau);
    // afficher une confirmation d'envoi du formulaire ou les erreurs de saisies
    if (! $erreurs) {
      header("Location: confirmation.php", true, 303);
      exit;
     
    } else {
    echo "<p style=\"color:red;\">ATTENTION!</p><p style=\"color:red;\">";
    foreach ($erreurs as $erreur) {echo "{$erreur}<br/>";}
      echo "</p>";
    }
  }
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8"/>
        <title>Association professionnelle d'informaticiennes et d'informaticiens</title>
    </head>
    <body>
        <h1>Formulaire d'admission à l'APII</h1>
        <p>Complétez le présent formulaire afin de vous joindre à l'Association. Tous les champs sont obligatoires.</p>
        
        <!-- Le code en php dans les inputs du formulaire sert à afficher la valeur préalablement saisies.
             Ainsi, en cas d'erreur, l'utilisateur n'aura pas à remplir le formulaire en entier de nouveau.-->
        <form action="./index.php" method="POST">
            <label>Nom</label>
            <input type="text" name="nom" maxlength="40" 
              <?php if(isset($membre_nouveau['nom'])){echo "value=\"{$membre_nouveau['nom']}\"";}?> />
            <br/>
            <label>Prénom</label>
            <input type="text" name="prenom" maxlength="50" 
              <?php if(isset($membre_nouveau['prenom'])){echo "value=\"{$membre_nouveau['prenom']}\"";}?> />
            <br/>
            <label>Âge</label>
            <input type="text" name="age" maxlength="2" 
              <?php if(isset($membre_nouveau['age'])){echo "value=\"{$membre_nouveau['age']}\"";}?> />
            <br/>
            <p>Dernier diplôme obtenu</p>
            <label>Année de diplomation</label>
            <input type="text" name="anneeDiplome" maxlength="4" placeholder="ex. 2018" 
              <?php if(isset($membre_nouveau['anneeDiplome'])){echo "value=\"{$membre_nouveau['anneeDiplome']}\"";}?> />
            <br/>
            <label>Établissement</label>
            <select name="universite">
                <option value="uqam">Université du Québec à Montréal</option>
                <option value="umtl" <?php if($membre_nouveau['universite'] == 'umtl'){echo "selected=\"selected\"";}?>>
                  Université de Montréal</option>
                <option value="mcgill" <?php if($membre_nouveau['universite'] == 'mcgill'){echo "selected=\"selected\"";}?>>
                  McGill</option>
                <option value="concordia" <?php if($membre_nouveau['universite'] == 'concordia'){echo "selected=\"selected\"";}?>>
                  Condordia</option>
                <option value="sherbrooke" <?php if($membre_nouveau['universite'] == 'sherbrooke'){echo "selected=\"selected\"";}?>>
                  Université de Sherbrooke</option>
                <option value="laval" <?php if($membre_nouveau['universite'] == 'laval'){echo "selected=\"selected\"";}?>>
                  Université Laval</option>
                <option value="bishop" <?php if($membre_nouveau['universite'] == 'bishop'){echo "selected=\"selected\"";}?>>
                  Bishop's</option>
                <option value="ets" <?php if($membre_nouveau['universite'] == 'ets'){echo "selected=\"selected\"";}?>>
                  École de technologie supérieure</option>
            </select>
            <br/>
            <label>Nom du programme</label>
            <input type="text" name="programme" maxlength="40" 
              <?php if(isset($membre_nouveau['programme'])){echo "value=\"{$membre_nouveau['programme']}\"";}?> />
            <br/>
            <br/>
            <label>Type d'admission</label>
            <br/>
            <input type="radio" name="type" value="regulier"
              <?php if(!isset($membre_nouveau['parcours']) || $membre_nouveau['type'] == 'regulier'){echo "checked=\"checked\"";}?> />
              Membre régulier<br/>
            <input type="radio" name="type" value="or"
              <?php if($membre_nouveau['type'] == 'or'){echo "checked=\"checked\"";}?> />
              Membre or<br/>
            <input type="radio" name="type" value="observateur"
              <?php if($membre_nouveau['type'] == 'observateur'){echo "checked=\"checked\"";}?> />
              Membre observateur<br/>
            <input type="radio" name="type" value="junior"
              <?php if($membre_nouveau['type'] == 'junior'){echo "checked=\"checked\"";}?> />
              Membre junior<br/>
            <br/>
            <label>Veuillez décrire brièvement votre parcours professionnel<label>
            <br/>
            <textarea name="parcours" cols="60" rows="5"><?php if(isset($membre_nouveau['parcours'])){echo $membre_nouveau['parcours'];}?></textarea>
            <br/>
            <br/>
            <input type="submit" value="Soumettre" />
        </form>
    </body>
</html>